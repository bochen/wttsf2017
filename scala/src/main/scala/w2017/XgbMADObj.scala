package w2017


import ml.dmlc.xgboost4j.java.XGBoostError
import ml.dmlc.xgboost4j.scala.{DMatrix, EvalTrait, ObjectiveTrait}
import org.apache.commons.logging.{Log, LogFactory}

import scala.collection.mutable.ListBuffer


class MADObj extends ObjectiveTrait {
  private val logger: Log = LogFactory.getLog(classOf[MADObj])


  /**
    * user define objective function, return gradient and second order gradient
    *
    * @param predicts untransformed margin predicts
    * @param dtrain   training data
    * @return List with two float array, correspond to first order grad and second order grad
    */
  override def getGradient(predicts: Array[Array[Float]], dtrain: DMatrix)
  : List[Array[Float]] = {
    val nrow = predicts.length
    val gradients = new ListBuffer[Array[Float]]
    var labels: Array[Float] = null
    try {
      labels = dtrain.getLabel
    } catch {
      case e: XGBoostError =>
        logger.error(e)
        null
      case _: Throwable =>
        null
    }
    require(labels.length == nrow)
    val grad = new Array[Float](nrow)
    val hess = new Array[Float](nrow)


    for (i <- 0 until nrow) {
      val (a_grad, a_hess) = cal_grad_hess(labels(i), predicts(i)(0))
      grad(i) = a_grad
      hess(i) = a_hess
    }
    gradients += grad
    gradients += hess
    if (false){
    println("AAA")
    println(labels.mkString(" "))
    println(predicts.map(_.apply(0)).mkString(" "))
    gradients.foreach(u=>println(u.mkString(" ")))
    }
    gradients.toList
  }


  def cal_grad_hess(label: Float, pred: Float): (Float, Float) = {
    val x = pred
    val y = label

    val tanhxy = math.tanh(x - y)
    val grad = tanhxy
    val hess = -tanhxy * tanhxy + 1

//    val grad = (if (x>y) 1 else -1) //+ 0.1* (x-y)
//    val hess = 0 //+ 0.1

//    val grad = 1*(x-y)
//    val hess = 1

    (grad.toFloat, hess.toFloat)
  }


}

class MADEEvalError extends EvalTrait {

  val logger = LogFactory.getLog(classOf[SMAPEEvalError])

  var evalMetric: String = "MAD"

  /**
    * get evaluate metric
    *
    * @return evalMetric
    */
  override def getMetric: String = evalMetric

  /**
    * evaluate with predicts and data
    *
    * @param predicts predictions as array
    * @param dmat     data matrix to evaluate
    * @return result of the metric
    */
  override def eval(predicts: Array[Array[Float]], dmat: DMatrix): Float = {
    var error: Float = 0f
    var labels: Array[Float] = null
    try {
      labels = dmat.getLabel
    } catch {
      case ex: XGBoostError =>
        logger.error(ex)
        return -1f
    }
    val nrow: Int = predicts.length
    for (i <- 0 until nrow) {
      val l = labels(i)
      val p = predicts(i)(0)
      error += math.abs(l - p)
      //error += (l-p)*(l-p)
    }
    error / labels.length
    //math.sqrt(error/labels.length).toFloat
  }
}

