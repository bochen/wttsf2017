package w2017


import ml.dmlc.xgboost4j.java.XGBoostError
import ml.dmlc.xgboost4j.scala.{DMatrix, EvalTrait, ObjectiveTrait}
import org.apache.commons.logging.{Log, LogFactory}

import scala.collection.mutable.ListBuffer


class SMAPEObj extends ObjectiveTrait {
  private val logger: Log = LogFactory.getLog(classOf[SMAPEObj])


  /**
    * user define objective function, return gradient and second order gradient
    *
    * @param predicts untransformed margin predicts
    * @param dtrain   training data
    * @return List with two float array, correspond to first order grad and second order grad
    */
  override def getGradient(predicts: Array[Array[Float]], dtrain: DMatrix)
  : List[Array[Float]] = {
    val nrow = predicts.length
    val gradients = new ListBuffer[Array[Float]]
    var labels: Array[Float] = null
    try {
      labels = dtrain.getLabel
    } catch {
      case e: XGBoostError =>
        logger.error(e)
        null
      case _: Throwable =>
        null
    }
    require(labels.length==nrow)
    val grad = new Array[Float](nrow)
    val hess = new Array[Float](nrow)


    for (i <- 0 until nrow) {
      val (a_grad,a_hess) = cal_grad_hess(labels(i),predicts(i)(0))
      grad(i) = a_grad
      hess(i) = a_hess
    }
    gradients += grad
    gradients += hess
    gradients.toList
  }

  private[this] def logcosh(x:Double )= {
    if(math.abs(x)>10){
      math.abs(x)-0.69314718056
    } else {
      math.log(math.cosh(x))
    }
  }

  def cal_grad_hess(label: Float, pred: Float): (Float,Float) =  {
    //if (label==0 && pred ==0 ) return (0, 0)
    if (label==0   ) return (0, 0)
    val x=pred
    val y= label

    val absy=math.abs(y)

    val logcoshx=logcosh(x)
    val logcoshxy=logcosh(x-y)
    val a=logcoshx+absy
    val square = (a:Double) => a*a
    val tanhxy=math.tanh(x-y)
    val tanhx=math.tanh(x)

    val grad = (2*tanhxy /a  - 2*logcoshxy *tanhx  /square(a))

    val hess = (
      -2 * tanhxy*tanhxy  + 2 +
        2*logcoshxy/a*(tanhx*tanhx -1) +
        -4* tanhxy*tanhx /a  +
        +4 * logcoshxy /square(a)*(tanhx*tanhx)
      )/a
    (grad.toFloat,hess.toFloat)
  }

  def cal_grad_hess_bk(label: Float, pred: Float): (Float,Float) =  {
    //if (label==0 && pred ==0 ) return (0, 0)
    if (label==0   ) return (0, 0)
    val x=pred/math.abs(label)
    val y= if(label>0) 1.0 else -1.0

    val absy=math.abs(y)

    val logcoshx=logcosh(x)
    val logcoshxy=logcosh(x-y)
    val a=logcoshx+absy
    val square = (a:Double) => a*a
    val tanhxy=math.tanh(x-y)
    val tanhx=math.tanh(x)

    val grad = (2*tanhxy /a  - 2*logcoshxy *tanhx  /square(a)) /math.abs(label)

    val hess = (
      -2 * tanhxy*tanhxy  + 2 +
      2*logcoshxy/a*(tanhx*tanhx -1) +
      -4* tanhxy*tanhx /a  +
      +4 * logcoshxy /square(a)*(tanhx*tanhx)
      )/a/math.abs(label)
    (grad.toFloat,hess.toFloat)
  }


}

class SMAPEEvalError extends EvalTrait {

  val logger = LogFactory.getLog(classOf[SMAPEEvalError])

  var evalMetric: String = "SMAPE"

  /**
    * get evaluate metric
    *
    * @return evalMetric
    */
  override def getMetric: String = evalMetric

  /**
    * evaluate with predicts and data
    *
    * @param predicts predictions as array
    * @param dmat     data matrix to evaluate
    * @return result of the metric
    */
  override def eval(predicts: Array[Array[Float]], dmat: DMatrix): Float = {
    var error: Float = 0f
    var labels: Array[Float] = null
    try {
      labels = dmat.getLabel
    } catch {
      case ex: XGBoostError =>
        logger.error(ex)
        return -1f
    }
    val nrow: Int = predicts.length
    for (i <- 0 until nrow) {
      val l = labels(i)
      val p = predicts(i)(0)
      if (l == 0.0 && p == 0) {
        error += 0
      } else {
        error += 2* math.abs(l-p)/(math.abs(l)+math.abs(p))
      }
    }
    error / labels.length
  }
}

