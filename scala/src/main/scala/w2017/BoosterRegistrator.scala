package w2017
import com.esotericsoftware.kryo.{Kryo, Serializer}
import ml.dmlc.xgboost4j.scala.Booster
import org.apache.spark.serializer.KryoRegistrator

class BoosterRegistrator extends KryoRegistrator {
  def registerClasses(kryo: Kryo): Unit = {
    kryo.register (classOf[Booster])

  }
}