package w2017

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.ml.regression.{L1Regression, LinearRegression, SmapeRegression}
import org.apache.spark.sql.Dataset
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.apache.spark.sql.functions.{col,udf,avg}


/**
  * Created by bo on 8/6/17.
  */

class SmapeRegressionTest extends FlatSpec with Matchers with BeforeAndAfter with DataFrameSuiteBase with LazyLogging {

  val name = Utils.home + "/spark/data/mllib/sample_linear_regression_data.txt"

  def logInfo(s:String)={
    println("AAAA: "+s)
    logger.info(s)
  }

  val mae_elemwise = udf((pred:Float,label:Float ) => math.abs(pred-label))
  val smape_elemwise= udf((pred:Float,label:Float ) =>{
    if (pred==label && label == 0)
      0
    else
      2*math.abs(pred-label)/(math.abs(pred)+math.abs(label))
  })

  def evaluate2(result:Dataset[_]) ={
    //var result=lrModel.transform(df)
    val mae = result.select(avg(mae_elemwise(result("prediction"),result("label")))).first.toSeq.head
    val smape = result.select(avg(smape_elemwise(result("prediction"),result("label")))).first.toSeq.head
    (mae,smape)
  }

  "Spark SmapeRegression" should "work  " in {
    val training = spark.read.format("libsvm").load(name)

    val lr = new SmapeRegression()
      .setMaxIter(10)
      .setRegParam(0.001)
      .setElasticNetParam(1)
        .setEtaParam(0.1)

    // Fit the model
    val lrModel = lr.fit(training)

    // Print the coefficients and intercept for linear regression
    logInfo(s"Coefficients size: ${lrModel.coefficients.size} Intercept: ${lrModel.intercept}")
    logInfo(s"#None zero coefficients: ${lrModel.coefficients.numNonzeros}")
    // Summarize the model over the training set and print out some metrics
    val trainingSummary = lrModel.summary
    logInfo(s"objectiveHistory: [${trainingSummary.objectiveHistory.mkString(",")}]")
    trainingSummary.residuals.show()
    logInfo(s"numIterations: ${trainingSummary.totalIterations}")
    logInfo(s"RMSE: ${trainingSummary.rootMeanSquaredError}")
    logInfo(s"MAE: ${trainingSummary.meanAbsoluteError}")
    logInfo(s"r2: ${trainingSummary.r2}")
    val result = lrModel.transform(training)
    println(evaluate2(result))
  }


}