package w2017

/*
 Copyright (c) 2014 by Contributors

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import ml.dmlc.xgboost4j.scala.Booster
import ml.dmlc.xgboost4j.scala.spark.XGBoost
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf

object SparkWithDataFrame {
  def main(args: Array[String]): Unit = {
    if (false && args.length != 4) {
      println(
        "usage: program num_of_rounds num_workers training_path test_path")
      sys.exit(1)
    }
    // create SparkSession
    val sparkConf = new SparkConf().setAppName("XGBoost-spark-example")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        .set("spark.kryo.registrator","w2017.BoosterRegistrator")
    //sparkConf.registerKryoClasses(Array(classOf[Booster]))
    // val sqlContext = new SQLContext(new SparkContext(sparkConf))
    val sparkSession = SparkSession.builder().master("local[2]").config(sparkConf).getOrCreate()
    // create training and testing dataframes
    val numRound = 10

    // build dataset
    val inputTrainPath="../input/agaricus.txt.train"
    val inputTestPath="../input/agaricus.txt.test"
    val trainDF = sparkSession.sqlContext.read.format("libsvm").load(inputTrainPath)
    val testDF = sparkSession.sqlContext.read.format("libsvm").load(inputTestPath)
    // start training
    val paramMap = List(
      "eta" -> 0.1f,
      "max_depth" -> 2,
      "obj_type" -> "classification",
      "objective" -> "binary:logistic").toMap
    val xgboostModel = XGBoost.trainWithDataFrame(
      trainDF, paramMap, numRound, nWorkers = 2.toInt, useExternalMemory = true)
    // xgboost-spark appends the column containing prediction results
    xgboostModel.transform(testDF).show()
  }
}