package w2017

import ml.dmlc.xgboost4j.scala.{DMatrix, XGBoost}
import org.apache.commons.logging.{Log, LogFactory}
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

import scala.collection.mutable

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class XgbSMAPETest extends FlatSpec with Matchers with BeforeAndAfter   {
  private val logger: Log = LogFactory.getLog(classOf[XgbSMAPETest])

 "xgb" should "work with SMAPE objective function"  in {
   val trainMat = new DMatrix("../input/machine.txt.train")
   val k=2
    //trainMat.setLabel(trainMat.getLabel.map(u=> if (u==0) k else u))
   val testMat = new DMatrix("../input/machine.txt.test")
   //testMat.setLabel(testMat.getLabel.map(u=> if (u==0) k else u))


   val params = new mutable.HashMap[String, Any]()
   params += "eta" ->  1
   params += "max_depth" -> 3
   params += "silent" -> 1
   params += "lambda" -> 1
   //params += "base_score" -> 100
   params += "min_child_weight" ->0
   params += "gamma" -> 2
   params += "booster" -> "gbtree"



   val watches = new mutable.HashMap[String, DMatrix]
   watches += "train" -> trainMat
   watches += "test" -> testMat

   val round = 100
   // train a model
   if (true  ) {
     val booster2 = XGBoost.train(trainMat, params.toMap, round, watches.toMap, new SMAPEObj, new SMAPEEvalError)
     val pred = booster2.predict(testMat)
     val se=testMat.getLabel.zip(pred.map(_.apply(0))).map{case (u,v)=>math.pow(u-v,2)}
   }
   logger.info("###############")
   if (!true) {
     params += "eval_metric" -> "mae"
     params += "objective" -> "reg:linearl1"

     val booster1 = XGBoost.train(trainMat, params.toMap, round, watches.toMap, eval= new SMAPEEvalError)
     val pred = booster1.predict(testMat)

   }
   logger.info("###############")

   // train a model
   if (!true  ) {
     params += "eval_metric" -> "mae"
     val booster2 = XGBoost.train(trainMat, params.toMap, round, watches.toMap, new MADObj, new SMAPEEvalError )
     val pred = booster2.predict(testMat)
     val se=testMat.getLabel.zip(pred.map(_.apply(0))).map{case (u,v)=>math.pow(u-v,2)}
     pred.take(10).foreach (u=> println( u.mkString(" ")))
   }
 }

}
