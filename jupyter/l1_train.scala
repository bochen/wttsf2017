
import org.apache.spark.sql.functions._
val filename="/tmp/linear_X.parquet"
val X=spark.read.parquet(filename).cache()
val rawcandy=spark.read.option("header", true).csv("/tmp/xg_y_candidates.csv")
val toDate = udf((s:  String ) => s.tail.toInt)
val toFloat = udf((s:  String ) => s.toFloat)
val candy = rawcandy.withColumn("date", toDate($"date")).withColumn("visits", toFloat($"visits")).cache()
val make_y = udf((M:  Float, visits:Float) =>visits/M)
def make_Xy(D:Int)={
    val Xy=X.join(candy, X("date")+D === candy("date") && X("page") === candy("page"))
    val joined= Xy.withColumn("label",make_y(col("M"), col("visits")))
    joined.select(col("features"),col("label"))
}

import org.apache.spark.ml.regression.{L1Regression,L1RegressionModel}
import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Dataset

val mae_elemwise = udf((pred:Float,label:Float ) => math.abs(pred-label))
val smape_elemwise= udf((pred:Float,label:Float ) =>{
    if (pred==label && label == 0)
        0
    else
        2*math.abs(pred-label)/(math.abs(pred)+math.abs(label))
})

def evaluate1(result:Dataset[_]) ={
    //var result=lrModel.transform(df)
    val mae = result.select(avg(mae_elemwise(result("prediction"),result("label2")))).first.toSeq.head
    val smape = result.select(avg(smape_elemwise(result("prediction"),result("label2")))).first.toSeq.head
     (mae,smape)
}

def evaluate2(result:Dataset[_]) ={
    //var result=lrModel.transform(df)
    val mae = result.select(avg(mae_elemwise(result("prediction"),result("label")))).first.toSeq.head
    val smape = result.select(avg(smape_elemwise(result("prediction"),result("label")))).first.toSeq.head
     (mae,smape)
}

def g(x:Double,k:Float) = {
    if (!true && x==0)
        x
    else
        math.max(1/k,math.min(k,x))
}
def make_train(Xy:Dataset[_], k:Float=2.5f, sample:Boolean=true)={
    val df = Xy
    val _train=
        if(sample) 
            df.sample(false,0.0001).cache()
        else
            df
    
    val f1 = udf((v:  Vector ) => Vectors.dense(v.toArray.map(x=>g(x,k))))
    val f2 = udf((x:   Float  ) =>  g(x,k))
    val train1= _train.withColumn("features2", f1(col("features")))
    val train2= train1.withColumn("label2", f2(col("label")))

    val train=train2
    train 
}

val DList=1.to(60)

val KList=(Array(3.5f, 3f, 2.5f,2.25f,2.0f,2.0f,2.0f,2.0f,2.0f,2.0f) ++ 0.to(100).map(_=>1.5f)).take(60)

(KList.length,DList.length)

def process_one(i:Int)={
    println(s"Start D${DList(i)}\n")
    val train=make_train(Xy=make_Xy(DList(i)), k=KList(i),sample=false).cache()
    print (s"Train size: ${train.count}")
    val f11 = udf((v:  Float) => if(v==0.0f) 0f else 1.0)
    //train.select(avg( f11($"label") )).show()
    val lr = new L1Regression()
      .setMaxIter(10)
      .setRegParam(0.001)
    val lrModel = lr.setLabelCol("label2").setFeaturesCol("features2").fit(train.filter($"label" > 0))

    val trainingSummary = lrModel.summary
    println(s"numIterations: ${trainingSummary.totalIterations}")
    println(s"objectiveHistory: ${trainingSummary.objectiveHistory.toList}")
    println(s"RMSE: ${trainingSummary.rootMeanSquaredError}")
    println(s"r2: ${trainingSummary.r2}")
    var result1=lrModel.transform(train).cache()
    println(s"MAD, SMAPE on label2:  ${evaluate1(result1)}")
    println(s"MAD, SMAPE on label:  ${evaluate2(result1)}")
    val modelfilename=s"/tmp/wl1/model_D${DList(i)}.lr"
    lrModel.write.overwrite.save(modelfilename)
    println(s"saved model to $modelfilename")
    
    result1.unpersist()    
    train.unpersist()    
    println(s"End D${DList(i)}\n") 
}

val idx=  (1 to 60)
idx.foreach(i=>process_one(i-1))

