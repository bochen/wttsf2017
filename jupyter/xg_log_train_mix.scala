import ml.dmlc.xgboost4j.scala.Booster
import ml.dmlc.xgboost4j.scala.spark.XGBoost
import ml.dmlc.xgboost4j.scala.spark.XGBoostModel

import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset
import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset

import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions._
    
val filename1="/tmp/xgb_log_mix_2"
val X20=spark.read.parquet(filename1).cache()
val X21=X20.withColumn("diff", $"target_date"-$"date")
val make_feature = udf((feats:  DenseVector, diff:Int ) =>Vectors.dense(Array(diff.toDouble ) ++ feats.toArray))
val X22=X21.withColumn("features", make_feature($"features",$"diff" ))    
    
    
    
val mae_elemwise = udf((pred:Float,label:Float ) => math.abs(pred-label))
val smape_elemwise= udf((pred:Float,label:Float ) =>{
    if (pred==label && label == 0)
        0
    else
        2*math.abs(pred-label)/(math.abs(pred)+math.abs(label))
})

def evaluate1(result:Dataset[_]) ={
    //var result=lrModel.transform(df)
    val mae = result.filter($"label">0).select(avg(mae_elemwise(result("prediction"),result("label2")))).first.toSeq.head
    val smape = result.filter($"label">0).select(avg(smape_elemwise(result("prediction"),result("label2")))).first.toSeq.head
     (mae,smape)
}

def evaluate2(result:Dataset[_]) ={
    //var result=lrModel.transform(df)
    val mae = result.filter($"label">0).select(avg(mae_elemwise(result("prediction2"),result("label")))).first.toSeq.head
    val smape = result.filter($"label">0).select(avg(smape_elemwise(result("prediction2"),result("label")))).first.toSeq.head
     (mae,smape)
}

 

def make_train(df:Dataset[_], sample:Boolean=true)={

    val _train=
        if(sample) 
            df.sample(false,0.0001).cache()
        else
            df

        val f1 = udf((v:  Vector ) => v)
        val f2 = udf((x:   Float  ) =>  if(x==0) 0 else math.log(x) ) //better
    val train1= _train.withColumn("features2", f1(col("features")))
    val train2= train1.withColumn("label2", f2(col("label")))

    val train=train2
    //train
    train.randomSplit(Array(0.8,0.2))

}

def process_one( )={
    println(s"Start")
     val Array(train,test)=make_train(df=X22,sample=false)

    //val train=make_train(df=X22,sample=false).cache()
    val paramMap = List(
      "eta" -> 0.3f,
        //"lambda" -> 5,
      "max_depth" -> 13,
        "min_child_weight" -> 50,
           "base_score" -> 0,
       "gamma" -> 0,
      "obj_type"  -> "reg:linear",
        "subsample" -> 1, 
        "colsample_bytree" -> 1,         
        "objective" -> "reg:smoothl1"        
    ).toMap
    val numRound = 100
    
    val xgboostModel = XGBoost.trainWithDataFrame(
    train.filter($"label" > 0), paramMap, numRound, nWorkers=150, useExternalMemory = true, featureCol="features2", labelCol="label2")    

    val mae_elemwise = udf((pred:Float,label:Float ) => math.abs(pred-label))
    val smape_elemwise= udf((pred:Float,label:Float ) =>{
        if (pred==label && label == 0)
            0
        else
            2*math.abs(pred-label)/(math.abs(pred)+math.abs(label))
    })

  if(true){ 
    val pred_trans=udf((v:  Float ) => math.exp(v))
    //val pred_trans=udf((v:  Float ) => v)
    var result1=xgboostModel.transform(train.sample(false,0.3)).withColumn("prediction2", pred_trans(col("prediction"))).cache()
    println(s"MAD, SMAPE on label2:  ${evaluate1(result1)}")
    println(s"MAD, SMAPE on label:  ${evaluate2(result1)}")
    result1=xgboostModel.transform(test).withColumn("prediction2", pred_trans(col("prediction"))).cache()
    println(s"MAD, SMAPE on label2:  ${evaluate1(result1)}")
    println(s"MAD, SMAPE on label:  ${evaluate2(result1)}")
        
        
    result1.unpersist()        
   }

    val modelfilename=s"/tmp/wxgb/model_log_mix_2.xgb"
    xgboostModel.write.overwrite.save(modelfilename)

    train.unpersist()    
    println(s"End D\n")
  
}

process_one

System.exit(0)
    
