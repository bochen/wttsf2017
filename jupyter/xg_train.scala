import ml.dmlc.xgboost4j.scala.Booster
import ml.dmlc.xgboost4j.scala.spark.XGBoost
import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset

    val mae_elemwise = udf((pred:Float,label:Float ) => math.abs(pred-label))
val smape_elemwise= udf((pred:Float,label:Float ) =>{
    if (pred==label && label == 0)
        0
    else
        2*math.abs(pred-label)/(math.abs(pred)+math.abs(label))
})

def evaluate1(result:Dataset[_]) ={
    //var result=lrModel.transform(df)
    val mae = result.select(avg(mae_elemwise(result("prediction"),result("label2")))).first.toSeq.head
    val smape = result.select(avg(smape_elemwise(result("prediction"),result("label2")))).first.toSeq.head
     (mae,smape)
}

def evaluate2(result:Dataset[_]) ={
    //var result=lrModel.transform(df)
    val mae = result.select(avg(mae_elemwise(result("prediction2"),result("label")))).first.toSeq.head
    val smape = result.select(avg(smape_elemwise(result("prediction2"),result("label")))).first.toSeq.head
     (mae,smape)
}

def g(x:Double,k:Float) = {
    if (!true && x==0)
        x
    else
        math.max(1/k,math.min(k,x))
}

def make_train_test(filename:String, sample:Boolean=true)={
    val df = spark.read.parquet(filename) 
    val _train=
        if(sample) 
            df.sample(false,0.0001).cache()
        else
            df

    //val f = udf((s:  String ) => s.toFloat)
    //val train= _train.withColumn("lable", f(col("lable")))
    val k=1.5f
    
      //val f1 = udf((v:  Vector ) => Vectors.dense(v.toArray.map(x=>g(x,k))))
      //val f2 = udf((x:   Float  ) =>  g(x,k))
      

        
        val f1 = udf((v:  Vector ) => v)
         //val f2 = udf((x:   Float  ) =>  x.toDouble)
         //val f2 = udf((x:   Float  ) =>  if(x>1) 2-1/x else  x ) //not good
        //val f2 = udf((x:   Float  ) =>  if(x<1 && x>0) -1/x else  x ) //bad
        val f2 = udf((x:   Float  ) =>  if(x==0) 0 else math.log(x) ) //better
    val train1= _train.withColumn("features2", f1(col("features")))
    val train2= train1.withColumn("label2", f2(col("label")))

    val train=train2
    train.randomSplit(Array(0.8,0.2))
}

val D=30
val filename="/tmp/xgb_day"+D+".parquet"


val Array(train,test)=make_train_test(filename,sample=false)

val paramMap = List(
  "eta" -> 0.3f,
    //"lambda" -> 5,
  "max_depth" -> 6,
    //"booster" -> "gblinear",
    //"min_child_weight" -> 0,
       "base_score" -> 1,
   "gamma" -> 0,
  "eval_metric"  -> "smape",
  "obj_type"  -> "reg:linear",
  "objective" -> "reg:smoothl1").toMap
val numRound = 100
    
val xgboostModel = XGBoost.trainWithDataFrame(
  train.filter($"label" > 0).filter($"X1zero"<0.4), paramMap, numRound, nWorkers=150, useExternalMemory = true, featureCol="features2", labelCol="label2")    

val mae_elemwise = udf((pred:Float,label:Float ) => math.abs(pred-label))
val smape_elemwise= udf((pred:Float,label:Float ) =>{
    if (pred==label && label == 0)
        0
    else
        2*math.abs(pred-label)/(math.abs(pred)+math.abs(label))
})

def evaluate(df:Dataset[_]) ={
    var result=xgboostModel.transform(df)
    val mae = result.select(avg(mae_elemwise(result("prediction"),result("lable")))).first.toSeq.head
    val smape = result.select(avg(smape_elemwise(result("prediction"),result("lable")))).first.toSeq.head
     (mae,smape)
}

val pred_trans=udf((v:  Float ) => math.exp(v))
//val pred_trans=udf((x:  Float ) => if (x>1) (2-1.0/x) else x)
//val pred_trans=udf((v:  Float ) => v)
var result1=xgboostModel.transform(train).cache()
result1=result1.withColumn("prediction2", pred_trans(col("prediction")))
    println(s"MAD, SMAPE on label2:  ${evaluate1(result1)}")
    println(s"MAD, SMAPE on label:  ${evaluate2(result1)}")
var result2=xgboostModel.transform(test).cache()
result2=result2.withColumn("prediction2", pred_trans(col("prediction")))

    println(s"MAD, SMAPE on label2:  ${evaluate1(result2)}")
    println(s"MAD, SMAPE on label:  ${evaluate2(result2)}")


//System.exit(0)
    