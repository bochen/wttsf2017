import ml.dmlc.xgboost4j.scala.Booster
import ml.dmlc.xgboost4j.scala.spark.XGBoost
import ml.dmlc.xgboost4j.scala.spark.XGBoostModel
import ml.dmlc.xgboost4j.scala.spark.XGBoostClassificationModel
    
import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset
import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset

import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions._
    
val filename1="/tmp/xgb_prb_mix_2"
val X20=spark.read.parquet(filename1).cache()
val X21=X20.withColumn("diff", $"target_date"-$"date")
val make_feature = udf((feats:  DenseVector) => feats)
val make_y = udf((i:Double)=> if (i>1) 1 else i)
val X22=X21.withColumn("features", make_feature($"features"))//.withColumn("label", make_y($"label"))    
    
    

def evaluate1(result1:Dataset[_]) ={
    val f=udf((a:Double,b:Double)=>if(a==b) 1 else 0)
    val accur=result1.select(avg(f($"label",$"pred"))).first.toSeq.head
    val accur0=result1.filter($"label"===0).select(avg(f($"label",$"pred"))).first.toSeq.head
    val accur1=result1.filter($"label"===1).select(avg(f($"label",$"pred"))).first.toSeq.head
    val accur2=result1.filter($"label"===2).select(avg(f($"label",$"pred"))).first.toSeq.head
    
    val g=udf((a:Seq[Double],b:Double)=>{
        val i = b.toInt
        -math.log(a(i)+1e-15)
    })
    val logloss=   result1.select(avg(g($"prob",$"label"))).first.toSeq.head
    
     (logloss,accur,(accur0,accur1,accur2))
}

 

def make_train(df:Dataset[_], sample:Boolean=true)={

    val _train=
        if(sample) 
            df.sample(false,0.0001).cache()
        else
            df

 
    val train=_train
    //train
    train.randomSplit(Array(0.8,0.2))

}

def process_one( )={
    println(s"Start")
     val Array(train,test)=make_train(df=X22,sample=false)

    //val train=make_train(df=X22,sample=false).cache()
    val paramMap = List(
      "eta" -> 0.1f,
        "colsample_bytree" -> 0.8,
        "subsample" -> 0.8,
        "max_depth" -> 6,
        //"eval_metric"  -> "mlogloss",
        //"obj_type"  -> "binary:logistic",
        //"objective" -> "binary:logistic"
        "objective" -> "multi:softmax",
        "num_class" -> 3
    ).toMap
    val numRound = 100
    
    val xgboostModel0 = XGBoost.trainWithDataFrame(
    train, paramMap, numRound, nWorkers=150, useExternalMemory = true, featureCol="features", labelCol="label")    
    val xgboostModel=xgboostModel0.asInstanceOf[XGBoostClassificationModel]
 

  if(true){ 
    val marginToProb= udf((margin:Vector) => {
        val v=margin.toArray
        val expv=v.map(math.exp _)
        val s = expv.sum
        expv.map(_/s)
    })
    val argmax=udf((probs:Seq[Double])=>{
        val m=probs.max
        probs.indices.filter{
            i=>
                 (probs(i)==m)  
        }.head
    })
    val f0=udf((probs:Seq[Double])=> probs.head)
    var result1=xgboostModel.setOutputMargin(true).setPredictionCol("").transform(train.sample(false,0.3)).withColumn("prob", marginToProb($"margin")).withColumn("prob0", f0($"prob")).withColumn("pred", argmax($"prob")).cache()      

    println(s"LOSS, ACCURACY on train:  ${evaluate1(result1)}")
    result1=xgboostModel.setOutputMargin(true).setPredictionCol("").transform(test).withColumn("prob", marginToProb($"margin")).withColumn("prob0", f0($"prob")).withColumn("pred", argmax($"prob")).cache()    
    println(s"LOSS, ACCURACY on test:  ${evaluate1(result1)}")
        
    result1.unpersist()        
   }

    val modelfilename=s"/tmp/wxgb/model_prb2_mix_2.xgb"
    xgboostModel.write.overwrite.save(modelfilename)

    train.unpersist()    
    println(s"End D\n")
  
}

process_one

System.exit(0)
    
